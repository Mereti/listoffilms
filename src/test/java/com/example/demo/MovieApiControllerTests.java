package com.example.demo;

import com.example.demo.service.MovieService;
import com.example.demo.service.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
class MovieApiControllerTests {

	private MovieService movieService = new MovieServiceImpl();

	private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiControllerTests.class);

}
