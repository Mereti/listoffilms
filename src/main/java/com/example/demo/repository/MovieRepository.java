package com.example.demo.repository;

import com.example.demo.MovieListDto;

public interface MovieRepository {
    MovieListDto getAllMovies();
}
