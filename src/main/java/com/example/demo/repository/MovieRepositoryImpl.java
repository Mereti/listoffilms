package com.example.demo.repository;

import com.example.demo.MovieListDto;
import com.example.demo.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

    private ArrayList<Movie> movieArrayList = new ArrayList<Movie>(Arrays.asList(
            new Movie(1,"Piraci z Krzemowej Doliny",1999,"https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"),
            new Movie(2,"Ja,robot",2004,"https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"),
            new Movie(3,"Kod nieśmiertelności",2011,"https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"),
            new Movie(4,"Ex Machina",2015,"https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg")
    ));

    private final MovieListDto movies = new MovieListDto(movieArrayList);

    public MovieListDto getAllMovies() {
        return this.movies ;
    }
}
