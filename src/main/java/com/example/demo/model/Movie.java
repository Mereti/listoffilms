package com.example.demo.model;

public class Movie {
    private Integer id;
    private String title;
    private Integer year;
    private String http;

    public Movie(){}

    public Movie(Integer id, String title, Integer year, String http) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.http = http;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }
}
