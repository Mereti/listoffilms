package com.example.demo;

import com.example.demo.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class MovieApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    @Autowired
    private MovieService movieService;
    
    @CrossOrigin
    @GetMapping(value="/movies")
    public ResponseEntity<MovieListDto> getAllMovies(){
        MovieListDto movies = movieService.getAllMovies();
        LOGGER.info("--- get all movies: {}",movies);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
    
}
