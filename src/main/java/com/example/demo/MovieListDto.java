package com.example.demo;

import com.example.demo.model.Movie;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private ArrayList<Movie> movies;

    public MovieListDto(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public ArrayList<Movie> getAllMovies() {
        return movies;
    }

    @Override
    public String toString() {
        return "[" + movies.stream()
                .map(movie -> movie.getId().toString())
                .collect(Collectors.joining(",")) + "]";
    }
    //TODO:sprawdzic czy zadziala powyzszy kod ze stream!

    public void setAllMovies(List<Movie> movies) {
        this.movies = (ArrayList<Movie>) movies;
    }


}