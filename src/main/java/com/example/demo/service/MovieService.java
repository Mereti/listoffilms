package com.example.demo.service;

import com.example.demo.MovieListDto;

public interface MovieService {
    MovieListDto getAllMovies();

}
