package com.example.demo.service;

import com.example.demo.MovieListDto;
import com.example.demo.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService{

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public MovieListDto getAllMovies(){return movieRepository.getAllMovies();}

}
